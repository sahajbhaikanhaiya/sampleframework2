

Pod::Spec.new do |s|


  s.name         = "SampleFramework2"
  s.version      = "0.0.1"
  s.summary      = "A short description of SampleFramework2."

  s.description  = "a long description of the ample framework very good framework"

  s.homepage     = "http://gogle.com/SampleFramework2"


  s.license      = "MIT"

  s.author       = { "Sahaj" => "sahaj.bhaikanhaiya@gmail.com" }

  
  s.platform   = :ios

  s.ios.deployment_target = '9.0'
  s.source       = { :http => "https://bitbucket.org/sahajbhaikanhaiya/sampleframework2/raw/d440b5555af03c18b903edcc742dc3e0c10a6a93/SampleFramework2.zip", :flatten =>true }


  s.frameworks  = "SampleFramework2"

  s.preserve_paths = "SampleFramework2.framework" 
  s.vendored_frameworks = "SampleFramework2.framework" 

end
